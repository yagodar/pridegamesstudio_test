﻿using System;
using System.Collections.Generic;

public class PoolTrimmer
{
    public PoolTrimmer(int poolTrimObjectsIterationLimit, float poolTrimPeriodSeconds)
    {
        this.poolTrimObjectsIterationLimit = poolTrimObjectsIterationLimit;
        this.poolTrimPeriodSeconds = poolTrimPeriodSeconds;
        poolTrimTimeSeconds = poolTrimPeriodSeconds;
        poolTrimQueue = new Queue<IPool>();
    }

    public void RegisterPool(IPool pool)
    {
        poolTrimQueue.Enqueue(pool);
    }

    public bool TryTrim(float deltaTime, Action<IPool> onTrimAction = null)
    {
        if (poolTrimTimeSeconds > 0.0f)
        {
            poolTrimTimeSeconds -= deltaTime;

            return false;
        }
        else
        {
            IPool pool;
            int poolTrimQueueCount = poolTrimQueue.Count;
            int trimmedPoolCount = 0;
            int iterationObjectCount = 0;
            while (trimmedPoolCount < poolTrimQueueCount && iterationObjectCount < poolTrimObjectsIterationLimit)
            {
                pool = poolTrimQueue.Dequeue();
                iterationObjectCount += pool.Count;
                pool.Trim();
                if(onTrimAction != null)
                {
                    onTrimAction.Invoke(pool);
                }
                ++trimmedPoolCount;
                poolTrimQueue.Enqueue(pool);
            }
            poolTrimTimeSeconds = poolTrimPeriodSeconds;

            return true;
        }
    }



    float poolTrimTimeSeconds;

    readonly int poolTrimObjectsIterationLimit;
    readonly float poolTrimPeriodSeconds;

    readonly Queue<IPool> poolTrimQueue;
}
