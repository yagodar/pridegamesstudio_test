﻿using System;
using UnityEngine;

public class PooledBehaviour<T> : IPooledBehaviour where T : Behaviour
{
    public int? Id { get; private set; }
    public T Behaviour { get; private set; }
    Behaviour IPooledBehaviour.Behaviour { get { return Behaviour; } }

    public PooledBehaviour(T behaviour)
    {
        Behaviour = behaviour;
    }

    public bool IsFree()
    {
        return isFree;
    }

    public long GetLastFreeTicks()
    {
        return lastFreeTicks;
    }

    public void Init()
    {
        Behaviour.gameObject.SetActive(false);
        isFree = true;
        lastFreeTicks = DateTime.UtcNow.Ticks;
    }

    public void Take(int? id = null)
    {
        Id = id;
        Behaviour.gameObject.SetActive(true);
        isFree = false;
    }

    public void Free()
    {
        Id = null;
        Behaviour.gameObject.SetActive(false);
        isFree = true;
        lastFreeTicks = DateTime.UtcNow.Ticks;
    }

    public void Destroy()
    {
        UnityEngine.Object.Destroy(Behaviour.gameObject);
    }



    private bool isFree;
    private long lastFreeTicks;
}
