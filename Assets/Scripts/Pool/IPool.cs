﻿public interface IPool
{
    int Count { get; }

    void Trim();

    void Clear();
}
