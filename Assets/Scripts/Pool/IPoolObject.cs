﻿public interface IPoolObject
{
    IPooledObject PooledObject { get; set; }
}
