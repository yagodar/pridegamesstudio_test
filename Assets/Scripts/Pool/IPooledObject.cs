﻿public interface IPooledObject
{
    bool IsFree();
    long GetLastFreeTicks();
    void Init();
    void Take(int? id = null);
    void Free();
    void Destroy();
}
