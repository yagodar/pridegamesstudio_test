﻿using UnityEngine;

public interface IPooledBehaviour : IPooledObject
{
    Behaviour Behaviour { get; }
}
