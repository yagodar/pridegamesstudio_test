﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DynamicObjectPool<T> : IPool where T : IPooledObject
{
    public int Count { get { return objects.Count; } }

    public DynamicObjectPool(Func<T> createFunc) : this(0, 0, createFunc, 0) { }

    public DynamicObjectPool(int initialCount, Func<T> createFunc) : this(0, initialCount, createFunc, 0) { }

    public DynamicObjectPool(int initialCapacity, int initialCount, Func<T> createFunc) : this(initialCapacity, initialCount, createFunc, 0) { }

    public DynamicObjectPool(int initialCapacity, int initialCount, Func<T> createFunc, long maxSpentFreeTicks)
    {
        //List - Ensuring the capacity is able to store another element:
        //int num = this._items.Length == 0 ? 4 : this._items.Length * 2;
        //Got this from the mscorlib 4.0.0.0 deassebled - of course, this cannot be guaranteed not to change in the future (so far it still stays at 0, 4, 8, 16...).
        objects = new List<T>(initialCapacity);

        this.createFunc = createFunc;

        for (int i = 0; i < initialCount; i++)
        {
            AddObject();
        }

        this.maxSpentFreeTicks = maxSpentFreeTicks;
    }

    public T Pop(int? id = null)
    {
        T obj;
        T pop = default;

        using (IEnumerator<T> enumerator = objects.GetEnumerator())
        {
            if (enumerator.MoveNext())
            {
                bool objFree;
                do
                {
                    obj = enumerator.Current;
                    objFree = obj.IsFree();
                    if (objFree)
                    {
                        pop = obj;
                    }
                }
                while (!objFree && enumerator.MoveNext());
            }
        }

        if (pop == null || pop.Equals(default(T)))
        {
            pop = AddObject();
        }

        pop.Take(id);

        return pop;
    }

    public void ForEachTookObject(Action<T> action)
    {
        foreach (T obj in objects)
        {
            if (!obj.IsFree())
            {
                action.Invoke(obj);
            }
        }
    }

    public void ForEachTookObject(Func<T, bool> func)
    {
        bool process = true;
        T obj;
        for (int i = 0; i < objects.Count && process; i++)
        {
            obj = objects[i];
            if (!obj.IsFree())
            {
                process = func.Invoke(obj);
            }
        }
    }

    public void Free(Func<T, bool> predicate)
    {
        foreach (var obj in objects)
        {
            if (!obj.IsFree() && predicate.Invoke(obj))
            {
                obj.Free();
            }
        }
    }

    public void Trim()
    {
        if (maxSpentFreeTicks > 0)
        {
            using (IEnumerator<T> enumerator = objects.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    long currentTimeTicks = DateTime.UtcNow.Ticks;
                    long lastFreeTicks;
                    int maxTrimObjCount = Mathf.FloorToInt(MaxTrimObjCountMod * objects.Count);
                    ICollection<T> trimObjs = new List<T>();
                    T obj;
                    while (trimObjs.Count < maxTrimObjCount && enumerator.MoveNext())
                    {
                        obj = enumerator.Current;
                        if (obj.IsFree())
                        {
                            lastFreeTicks = obj.GetLastFreeTicks();
                            if (lastFreeTicks > 0 && currentTimeTicks - lastFreeTicks > maxSpentFreeTicks)
                            {
                                obj.Destroy();
                                trimObjs.Add(obj);
                            }
                        }
                    }
                    foreach (var trimObj in trimObjs)
                    {
                        objects.Remove(trimObj);
                    }
                }
            }
        }
    }

    public void Clear()
    {
        objects.Clear();
    }



    T AddObject()
    {
        var obj = createFunc.Invoke();

        objects.Add(obj);

        obj.Init();

        return obj;
    }

    readonly List<T> objects;
    readonly Func<T> createFunc;
    readonly long maxSpentFreeTicks;

    const float MaxTrimObjCountMod = 0.75f;
}
