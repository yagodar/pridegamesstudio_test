﻿using UnityEngine;

public class GraphicEffect : MonoBehaviour,
    IPoolObject
{
    public IPooledObject PooledObject { get; set; }

    public Transform Transform => mTransform;

    public void Play()
    {
        if(mParticleSystem != null)
        {
            mParticleSystem.Play();
        }
    }
    


    private void OnParticleSystemStopped() => PooledObject.Free();

    [SerializeField]
    private Transform mTransform = default;
    [SerializeField]
    private ParticleSystem mParticleSystem = default;
}
