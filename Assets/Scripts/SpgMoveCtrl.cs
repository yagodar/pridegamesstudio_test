﻿using UnityEngine;
using UnityEngine.InputSystem;

public class SpgMoveCtrl : MonoBehaviour
{
    public Rigidbody SpgRigidbody => spgRigidbody;

    public void Initialize(Rigidbody spgRigidbody)
    {
        this.spgRigidbody = spgRigidbody;

        if (spgRigidbody != null)
        {
            spgRigidbody.centerOfMass = new Vector3(0.0f, 2.5f, 0.0f);
        }
        else
        {
            Debug.LogError(name + " NOT properly set!");
        }
    }

    public void HandleMove(InputAction.CallbackContext context)
    {
        moveAxis = context.ReadValue<Vector2>();
    }

    public void HandleAutoSpin(float directionValue)
    {
        autospinAxisValue = directionValue;
    }

    public void CancelMove(InputAction.CallbackContext context)
    {
        moveAxis = Vector2.zero;
    }



    private void FixedUpdate()
    {
        if(spgRigidbody == null)
        {
            return;
        }

        if (grounded)
        {
            if (spgRigidbody.velocity.magnitude < maxSpeed)
            {
                Vector3 direction = moveAxis.y * transform.forward;
                spgRigidbody.AddForce(power * direction, ForceMode.Force);
            }

            Vector3 rotateEulerAngles = turnSpeed * Time.fixedDeltaTime * Mathf.Clamp(moveAxis.x + autospinAxisValue, -1.0f, 1.0f) * transform.up;
            spgRigidbody.MoveRotation(Quaternion.Euler(transform.rotation.eulerAngles + rotateEulerAngles));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        grounded = true;
    }

    void OnCollisionStay(Collision collision)
    {
        grounded = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        grounded = false;
    }

    private Rigidbody spgRigidbody;    

    private bool grounded;

    private Vector2 moveAxis;
    private float autospinAxisValue;

    [SerializeField]
    private float turnSpeed = 100.0f;
    [SerializeField]
    private float maxSpeed = 50.0f;
    [SerializeField]
    private float power = 25000.0f;
}
