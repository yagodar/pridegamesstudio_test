﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SpgGunCtrl : MonoBehaviour
{
    public void Initialize(SpgMoveCtrl spgMoveCtrl, Camera spgCamera, DynamicObjectPool<PooledBehaviour<Shell>> shellPool,
        DynamicObjectPool<PooledBehaviour<GraphicEffect>> fireEffectPool, DynamicObjectPool<PooledBehaviour<GraphicEffect>> blowEffectPool)
    {
        this.spgMoveCtrl = spgMoveCtrl;
        this.spgCamera = spgCamera;
        this.shellPool = shellPool;
        this.fireEffectPool = fireEffectPool;
        this.blowEffectPool = blowEffectPool;

        terrainLayerMask = LayerMask.GetMask("Terrain");
        terrainTag = "Terrain";

        if (spgMoveCtrl == null
            || spgCamera == null
            || shellPool == null
            || fireEffectPool == null
            || blowEffectPool == null
            || gunTipTransform == null
            || gunTransform == null
            || lineRenderer == null
            || cursorTransform == null
            || cursorSpriteRenderer == null)
        {
            Debug.LogError(name + " NOT properly set!");
        }
    }
    
    public void HandleShoot(InputAction.CallbackContext context)
    {
        if(isValidTargetPosition)
        {
            if (shellPool != null)
            {
                PooledBehaviour<Shell> polledShell = shellPool.Pop();

                Shell shell = polledShell.Behaviour;

                if (gunTipTransform != null)
                {
                    Vector3 shellVelocity = bulletSpeed * gunTipTransform.up;
                    GraphicEffect fireEffect = fireEffectPool?.Pop().Behaviour;
                    GraphicEffect blowEffect = blowEffectPool?.Pop().Behaviour;
                    shell.Fire(gunTipTransform.position, shellVelocity, fireEffect, blowEffect);
                }
                else
                {
                    polledShell.Free();

                    Debug.LogError(name + " NOT properly set!");
                }
            }
        }
    }



    private void FixedUpdate()
    {
        isValidTargetPosition = false;

        if (spgCamera != null && cursorTransform != null)
        {
            Ray ray = spgCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, terrainLayerMask))
            {
                Vector3 cursorPosition = hit.point;
                cursorPosition.y += cursorSurfaceOffset;
                cursorTransform.position = cursorPosition;

                Vector3 newCursorForwardLookRotation;
                if (spgMoveCtrl != null && spgMoveCtrl.SpgRigidbody != null)
                {
                    newCursorForwardLookRotation = spgMoveCtrl.SpgRigidbody.transform.forward;
                }
                else
                {
                    newCursorForwardLookRotation = cursorTransform.forward;
                }

                cursorTransform.rotation = Quaternion.LookRotation(hit.normal, newCursorForwardLookRotation);

                if (gunTransform != null && hit.collider.CompareTag(terrainTag))
                {
                    float prevGunXAngle = gunTransform.localEulerAngles.x;

                    gunTransform.LookAt(hit.point, spgMoveCtrl.SpgRigidbody.transform.up);

                    float newGunXAngle;
                    float newGunYAngle = gunTransform.localEulerAngles.y;

                    float cosNewGunYAngle = Mathf.Cos(Mathf.Deg2Rad * newGunYAngle);
                    float cosNewGunMinYAngle = Mathf.Cos(Mathf.Deg2Rad * aimingAngleXMin);
                    float cosNewGunMaxYAngle = Mathf.Cos(Mathf.Deg2Rad * aimingAngleXMax);

                    float sinNewGunYAngle = Mathf.Sin(Mathf.Deg2Rad * newGunYAngle);

                    isValidTargetPosition = cosNewGunYAngle >= cosNewGunMinYAngle && cosNewGunYAngle >= cosNewGunMaxYAngle;
                    
                    float spgAutospinDirectionValue = 0.0f;
                    if (!isValidTargetPosition)
                    {
                        if(sinNewGunYAngle < 0.0f)
                        {
                            spgAutospinDirectionValue = -1.0f;
                            newGunYAngle = aimingAngleXMin;
                        }
                        else
                        {
                            spgAutospinDirectionValue = 1.0f;
                            newGunYAngle = aimingAngleXMax;
                        }                        
                    }
                    spgMoveCtrl.HandleAutoSpin(spgAutospinDirectionValue);

                    isValidTargetPosition &= TryGetHighAngleToHitTarget(out float highAngle);

                    if (isValidTargetPosition)
                    {
                        isValidTargetPosition &= highAngle >= aimingAngleYMin && highAngle <= aimingAngleYMax;

                        if(!isValidTargetPosition)
                        {
                            if(highAngle < aimingAngleYMin)
                            {
                                highAngle = aimingAngleYMin;
                            }
                            else
                            {
                                highAngle = aimingAngleYMax;
                            }                            
                        }

                        newGunXAngle = 90f - highAngle;
                    }
                    else
                    {
                        newGunXAngle = prevGunXAngle;
                    }

                    gunTransform.localEulerAngles = new Vector3(newGunXAngle, newGunYAngle, gunTransform.localEulerAngles.z);
                }

                if (lineRenderer != null)
                {
                    if (isValidTargetPosition)
                    {
                        DrawTrajectoryPath(
                            cursorTransform.position,
                            gunTipTransform.up * bulletSpeed,
                            gunTipTransform.position
                        );
                    }
                    else
                    {
                        lineRenderer.positionCount = 0;
                    }
                }
            }
        }

        if (cursorSpriteRenderer != null)
        {
            cursorSpriteRenderer.color = isValidTargetPosition ? Color.green: Color.red;
        }
    }

    private bool TryGetHighAngleToHitTarget(out float highAngle)
    {
        bool result = false;

        highAngle = 0.0f;

        if (cursorTransform != null && gunTipTransform != null)
        {
            Vector3 translation = cursorTransform.position - gunTipTransform.position;
            float verticalDistance = translation.y;
            translation.y = 0f; //Reset y to get the horizontal distance x
            float horizontalDistance = translation.magnitude;

            float gravity = -Physics.gravity.y;

            float speedSqr = bulletSpeed * bulletSpeed;

            float underTheRoot = (speedSqr * speedSqr) - gravity * (gravity * horizontalDistance * horizontalDistance + 2 * verticalDistance * speedSqr);
            if (underTheRoot >= 0f)
            {
                float rightSide = Mathf.Sqrt(underTheRoot);
                float top = speedSqr + rightSide;
                float bottom = gravity * horizontalDistance;

                highAngle = Mathf.Atan2(top, bottom) * Mathf.Rad2Deg;

                result = true;
            }
        }

        return result;
    }
        
    private void DrawTrajectoryPath(Vector3 targetPosition, Vector3 shellStartVelocity, Vector3 shellStartPosition)
    {
        List<Vector3> pathPoints = new List<Vector3>();
        pathPoints.Add(shellStartPosition);

        Vector3 currentVelocity = shellStartVelocity;
        Vector3 currentPosition = shellStartPosition;

        Vector3 newPosition;
        float timeToHitTarget = 0.0f;
        bool endRiched = false;
        while (!endRiched && timeToHitTarget < 30f)//Limit to 30 seconds to avoid infinite loop if never reach the target
        {
            ProcessHeunsIntegration(Time.fixedDeltaTime, currentPosition, currentVelocity, out newPosition, out Vector3 newVelocity);

            pathPoints.Add(newPosition);

            endRiched = newPosition.y < currentPosition.y && newPosition.y < targetPosition.y;

            currentPosition = newPosition;
            currentVelocity = newVelocity;

            timeToHitTarget += Time.fixedDeltaTime;
        } 

        lineRenderer.positionCount = pathPoints.Count;
        for (int i = 0; i < pathPoints.Count; i++)
        {
            lineRenderer.SetPosition(i, pathPoints[i]);
        }
    }

    private static void ProcessHeunsIntegration(float deltaTime,
        Vector3 currentPosition,
        Vector3 currentVelocity,
        out Vector3 newPosition,
        out Vector3 newVelocity)
    {
        Vector3 acceleartionFactorEuler = Physics.gravity;
        Vector3 acceleartionFactorHeun = Physics.gravity;
                       
        Vector3 velocityFactor = currentVelocity;
        //Wind velocity
        //velocityFactor += new Vector3(2f, 0f, 3f);


        //
        //Main algorithm
        //
        //Euler forward
        Vector3 pos_E = currentPosition + deltaTime * velocityFactor;

        //acceleartionFactorEuler += CalculateDrag(currentVelocity);

        Vector3 vel_E = currentVelocity + deltaTime * acceleartionFactorEuler;


        //Heuns method
        Vector3 pos_H = currentPosition + deltaTime * 0.5f * (velocityFactor + vel_E);

        //acceleartionFactorHeun += CalculateDrag(vel_E);

        Vector3 vel_H = currentVelocity + deltaTime * 0.5f * (acceleartionFactorEuler + acceleartionFactorHeun);


        newPosition = pos_H;
        newVelocity = vel_H;
    }

    private SpgMoveCtrl spgMoveCtrl;
    private Camera spgCamera;
    private DynamicObjectPool<PooledBehaviour<Shell>> shellPool;
    private DynamicObjectPool<PooledBehaviour<GraphicEffect>> fireEffectPool;
    private DynamicObjectPool<PooledBehaviour<GraphicEffect>> blowEffectPool;

    private int terrainLayerMask;
    private string terrainTag;

    private bool isValidTargetPosition;

    [SerializeField]
    private float bulletSpeed = 50f;
    [SerializeField]
    private Transform gunTipTransform = default;
    [SerializeField]
    private Transform gunTransform = default;

    [Space]

    [SerializeField]
    private float aimingAngleXMin = -20.0f;
    [SerializeField]
    private float aimingAngleXMax = 20.0f;
    [SerializeField]
    private float aimingAngleYMin = 0.0f;
    [SerializeField]
    private float aimingAngleYMax = 80.0f;
    
    [Space]

    [SerializeField]
    private LineRenderer lineRenderer = default;

    [Space]

    [SerializeField]
    private float cursorSurfaceOffset = 0.4f;
    [SerializeField]
    private Transform cursorTransform = default;
    [SerializeField]
    private SpriteRenderer cursorSpriteRenderer = default;
}
