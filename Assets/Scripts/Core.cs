﻿using UnityEngine;

public class Core : MonoBehaviour
{
    private void Awake()
    {
        inputControls = new InputControls();

        if(shellPrefab != null)
        {
            InitializePools();
        }

        if (spgMoveCtrl != null)
        {
            spgMoveCtrl.Initialize(spgRigidbody);
        }

        if (spgGunCtrl != null)
        {
            spgGunCtrl.Initialize(spgMoveCtrl, spgCamera, shellPool, fireEffectPool, blowEffectPool);
        }   

        if (spgMoveCtrl == null
            || spgGunCtrl == null
            || shellPrefab == null)
        { 
            Debug.LogError(name + " NOT properly set!");
        }
    }

    private void OnEnable()
    {
        if (spgMoveCtrl != null)
        {
            inputControls.SelfPropelledGun.Move.performed += spgMoveCtrl.HandleMove;
            inputControls.SelfPropelledGun.Move.canceled += spgMoveCtrl.CancelMove;
        }

        if(spgGunCtrl != null)
        {
            inputControls.SelfPropelledGun.Shoot.performed += spgGunCtrl.HandleShoot;
        }

        inputControls.Enable();
    }

    private void OnDisable()
    {
        if (spgMoveCtrl != null)
        {
            inputControls.SelfPropelledGun.Move.performed -= spgMoveCtrl.HandleMove;
            inputControls.SelfPropelledGun.Move.canceled -= spgMoveCtrl.CancelMove;
        }

        if (spgGunCtrl != null)
        {
            inputControls.SelfPropelledGun.Shoot.performed -= spgGunCtrl.HandleShoot;
        }

        inputControls.Disable();
    }

    private void InitializePools()
    {   
        shellPool = new DynamicObjectPool<PooledBehaviour<Shell>>(5, 5, () => CreatePolledBehaviour(shellPrefab));
        fireEffectPool = new DynamicObjectPool<PooledBehaviour<GraphicEffect>>(5, 5, () => CreatePolledBehaviour(fireEffectPrefab));
        blowEffectPool = new DynamicObjectPool<PooledBehaviour<GraphicEffect>>(5, 5, () => CreatePolledBehaviour(blowEffectPrefab));
    }

    private PooledBehaviour<T> CreatePolledBehaviour<T>(T prefab) where T : Behaviour, IPoolObject
    {
        T behaviour = Instantiate(prefab);
        PooledBehaviour<T> pooledBehaviour = new PooledBehaviour<T>(behaviour);
        behaviour.PooledObject = pooledBehaviour;
        return pooledBehaviour;
    }

    private InputControls inputControls;
    private DynamicObjectPool<PooledBehaviour<Shell>> shellPool;
    private DynamicObjectPool<PooledBehaviour<GraphicEffect>> fireEffectPool;
    private DynamicObjectPool<PooledBehaviour<GraphicEffect>> blowEffectPool;
        
    [SerializeField]
    private Rigidbody spgRigidbody = default;

    [SerializeField]
    private Camera spgCamera = default;

    [Space]

    [SerializeField]
    private SpgMoveCtrl spgMoveCtrl = default;

    [SerializeField]
    private SpgGunCtrl spgGunCtrl = default;

    [Space]

    [SerializeField]
    private Shell shellPrefab = default;
    [SerializeField]
    private GraphicEffect fireEffectPrefab = default;
    [SerializeField]
    private GraphicEffect blowEffectPrefab = default;
}
