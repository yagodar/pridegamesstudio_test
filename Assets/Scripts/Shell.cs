﻿using UnityEngine;

public class Shell : MonoBehaviour,
    IPoolObject
{
    public IPooledObject PooledObject { get; set; }

    
    public void Fire(Vector3 startPosition, Vector3 fireVelocity, GraphicEffect fireEffect, GraphicEffect blowEffect)
    {
        if (mTransform != null)
        {
            this.blowEffect = blowEffect;

            mTransform.position = startPosition;
            mTransform.rotation = Quaternion.LookRotation(fireVelocity);            
        }
        else
        {
            this.blowEffect = null;

            Debug.LogError(name + " NO Transform!");
        }

        if (mRigidbody != null)
        {
            mRigidbody.velocity = fireVelocity;
            mRigidbody.angularVelocity = Vector3.zero;

            if (fireEffect != null && fireEffect.Transform != null)
            {
                fireEffect.Transform.position = startPosition;
                fireEffect.Play();
            }
        }
        else
        {
            Debug.LogError(name + " NO Rigidbody!");
        }
    }

    private void FixedUpdate()
    {
        mTransform.rotation = Quaternion.LookRotation(mRigidbody.velocity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(blowEffect != null && blowEffect.Transform != null && mTransform != null)
        {
            blowEffect.Transform.position = mTransform.position;
            blowEffect.Play();
        }
        PooledObject.Free();
    }

    private GraphicEffect blowEffect;

    [SerializeField]
    private Transform mTransform = default;
    [SerializeField]
    private Rigidbody mRigidbody = default;
}
