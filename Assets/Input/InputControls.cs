// GENERATED AUTOMATICALLY FROM 'Assets/Input/InputControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputControls : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @InputControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControls"",
    ""maps"": [
        {
            ""name"": ""SelfPropelledGun"",
            ""id"": ""654a18a0-9713-425b-9327-471778c23f36"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Button"",
                    ""id"": ""4dd889d4-1f8e-4742-b81a-6f4215db3b71"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Shoot"",
                    ""type"": ""Button"",
                    ""id"": ""e5848f7b-af32-41c0-b31a-79a9666ded5f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""d3577014-8ede-45d8-9abd-5ec1d023d738"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""c29ff75a-7921-4a28-8119-230f85d79f1e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""0817047a-024f-4155-ab76-3b3c298f709b"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""b48d8f77-8d84-4eb4-aa31-4fd4ba918fa7"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""396678ef-6b42-4cbd-a50f-1a2d2e3c6732"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""77f7efb2-447e-43ea-96e8-24aa912a46b4"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // SelfPropelledGun
        m_SelfPropelledGun = asset.FindActionMap("SelfPropelledGun", throwIfNotFound: true);
        m_SelfPropelledGun_Move = m_SelfPropelledGun.FindAction("Move", throwIfNotFound: true);
        m_SelfPropelledGun_Shoot = m_SelfPropelledGun.FindAction("Shoot", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // SelfPropelledGun
    private readonly InputActionMap m_SelfPropelledGun;
    private ISelfPropelledGunActions m_SelfPropelledGunActionsCallbackInterface;
    private readonly InputAction m_SelfPropelledGun_Move;
    private readonly InputAction m_SelfPropelledGun_Shoot;
    public struct SelfPropelledGunActions
    {
        private @InputControls m_Wrapper;
        public SelfPropelledGunActions(@InputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_SelfPropelledGun_Move;
        public InputAction @Shoot => m_Wrapper.m_SelfPropelledGun_Shoot;
        public InputActionMap Get() { return m_Wrapper.m_SelfPropelledGun; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(SelfPropelledGunActions set) { return set.Get(); }
        public void SetCallbacks(ISelfPropelledGunActions instance)
        {
            if (m_Wrapper.m_SelfPropelledGunActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_SelfPropelledGunActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_SelfPropelledGunActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_SelfPropelledGunActionsCallbackInterface.OnMove;
                @Shoot.started -= m_Wrapper.m_SelfPropelledGunActionsCallbackInterface.OnShoot;
                @Shoot.performed -= m_Wrapper.m_SelfPropelledGunActionsCallbackInterface.OnShoot;
                @Shoot.canceled -= m_Wrapper.m_SelfPropelledGunActionsCallbackInterface.OnShoot;
            }
            m_Wrapper.m_SelfPropelledGunActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Shoot.started += instance.OnShoot;
                @Shoot.performed += instance.OnShoot;
                @Shoot.canceled += instance.OnShoot;
            }
        }
    }
    public SelfPropelledGunActions @SelfPropelledGun => new SelfPropelledGunActions(this);
    public interface ISelfPropelledGunActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnShoot(InputAction.CallbackContext context);
    }
}
